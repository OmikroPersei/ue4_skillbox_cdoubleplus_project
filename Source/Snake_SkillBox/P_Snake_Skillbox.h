// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "P_Snake_Skillbox.generated.h"

class UCameraComponent;
class AA_Snake;

UCLASS()
class SNAKE_SKILLBOX_API AP_Snake_Skillbox : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AP_Snake_Skillbox();

	UPROPERTY(BlueprintReadWrite)
		UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
		AA_Snake* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AA_Snake> SnakeActorClass;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

	UFUNCTION()
		void HandlePlayerVerticalInput(float value);
	UFUNCTION()
		void HandlePlayerHorizontalInput(float value);
};
