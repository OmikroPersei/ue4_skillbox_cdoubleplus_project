// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Snake_SkillBoxGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_SKILLBOX_API ASnake_SkillBoxGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
