// Fill out your copyright notice in the Description page of Project Settings.


#include "A_Snake_Elements.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
AA_Snake_Elements::AA_Snake_Elements()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void AA_Snake_Elements::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AA_Snake_Elements::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AA_Snake_Elements::SetFirstElementType_Implementation()
{
}

