// Copyright Epic Games, Inc. All Rights Reserved.

#include "Snake_SkillBox.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Snake_SkillBox, "Snake_SkillBox" );
