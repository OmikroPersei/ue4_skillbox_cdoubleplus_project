// Fill out your copyright notice in the Description page of Project Settings.


#include "P_Snake_Skillbox.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "A_Snake.h"
#include "Components/InputComponent.h"

// Sets default values
AP_Snake_Skillbox::AP_Snake_Skillbox()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void AP_Snake_Skillbox::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
}

// Called every frame
void AP_Snake_Skillbox::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AP_Snake_Skillbox::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &AP_Snake_Skillbox::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &AP_Snake_Skillbox::HandlePlayerHorizontalInput);
}

void AP_Snake_Skillbox::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<AA_Snake>(SnakeActorClass, FTransform());
}

void AP_Snake_Skillbox::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection!=EMovementDirection::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void AP_Snake_Skillbox::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		}
	}
}

